interface interfaceStruct {
    just?: number
}

function sample(struct: interfaceStruct): void {
    let { just = 10 }: interfaceStruct = struct;
}
sample({ just: 45 });

