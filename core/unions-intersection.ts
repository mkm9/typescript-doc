// its helps to compose types

function padLeft(value: string, padding: any) {
    if (typeof padding === "number") {
        return Array(padding + 1).join(" ") + value;
    }
    if (typeof padding === "string") {
        return padding + value;
    }
    throw new Error(`Expected string or number, got '${typeof padding}'.`);
}

// padLeft("john", {});

// solution

function padLeftSolve(value: string, padding: string | number) {
    if (typeof padding === "number") {
        return Array(padding + 1).join(" ") + value;
    }
    if (typeof padding === "string") {
        return padding + value;
    }
    throw new Error(`Expected string or number, got '${typeof padding}'.`);
}

// use the vertical bar (|) to separate each type

// Unions with Common Fields-----------
// we can only access members that are common to all types in the union

interface Bird {
    fly(): void;
    layEggs(): void;
}

interface Fish {
    swim(): void;
    layEggs(): void;
}

function getSmallPet(leg: number): Fish | Bird {
    if (leg === 2) {
        return { fly: () => { }, layEggs: () => { } } as Bird;
    } else {
        return { swim: () => { }, layEggs: () => { } } as Fish;
    }
}



let pet = getSmallPet(4);
pet.layEggs();
// pet.swim(); // bcz compiler has not clarity is instance of Bird or Fish

(pet as Fish).swim();

// Discriminating Unions --------------
type NetworkLoadingState = {
    state: "loading";
};
type NetworkFailedState = {
    state: "failed";
    code: number;
};
type NetworkSuccessState = {
    state: "success";
    response: {
        title: string;
        duration: number;
        summary: string;
    };
};
// Create a type which represents only one of the above types
// but you aren't sure which it is yet.
type NetworkState =
    | NetworkLoadingState
    | NetworkFailedState
    | NetworkSuccessState;

function logger1(state: NetworkState): string {
    // Right now TypeScript does not know which of the three
    // potential types state could be.

    // Trying to access a property which isn't shared
    // across all types will raise an error
    // state.code; // gives error


    // By switching on state, TypeScript can narrow the union
    // down in code flow analysis
    switch (state.state) {
        case "loading":
            return "Downloading...";
        case "failed":
            // The type must be NetworkFailedState here,
            // so accessing the `code` field is safe
            return `Error ${state.code} downloading`;
        case "success":
            return `Downloaded ${state.response.title} - ${state.response.summary}`;
    }
}

// Intersection Types ------------
// types has all member of intersection interface or types
interface ErrorHandling {
    isSuccess: boolean;
    error?: { message: string };
}

interface ArtworksData {
    artworks: { title: string }[];
}

interface ArtistsData {
    artists: { name: string }[];
}

// These interfaces are composed to have
// consistent error handling, and their own data.

type ArtworksResponse = ArtworksData & ErrorHandling;
type ArtistsResponse = ArtistsData & ErrorHandling;
