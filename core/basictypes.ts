// basically every programing language have units of data: numbers, strings, structures, boolean

//  boolean ---------------
//  it common data type in every language

let isDone: boolean = false;
let isOk = false;


//  Number ---------------
//  type script has to time in NUmber
//  Number are include all floating points- number
//  only BigIntegers - bigint

let decimal: number = 6;
let hex: number = 0xf00d;
let binary: number = 0b1010;
let octal: number = 0o744;
// let big: bigint = 100n; // bigint only support ES2020 and prior


//  String ---------------
//  it is same as javascript
//  it use single quotes('), double quotes ("") and template String (`)

let fullName: string = `Bob Bobbington`;
let sentence: string = `Hello, my name is ${fullName}.`;


//  Array ---------------
//  arry can define two ways


let arr1: number[] = [1, 2, 3];
let arr2: Array<string> = ["manoj", "akash"]
let arr3: (number | string)[] = ["abc", 4566] //  you should make multiple type data in one array (my observation)


//  Tuple ---------------
//  it is an array with a fixed number of elements whose types are known, but need not be the same.

let tup1: [string, number] = ["manoj", 24];

// error example
// tup1 = [1, "manoj"] 
// tup1[3] = "world";
// console.log(tup1[1].substring(1)); 
// console.log(tup1[5].toString());

// ---------------------------------------- New data type in typescript -----------------------------------------------------------

// ENUM ---------------
// it is like C# Enum
// it start with 0 value

enum Gender {
    Male,
    Female,
    Other
}
// console.log("Gender.Male", Gender.Male)

// you can manual set Value
// also you can set all value

enum GenderCopy {
    Male = 2,
    Female,
    Other
}
// console.log("GenderCopy.Female", GenderCopy.Female)

let x: string;
// x = GenderCopy.Male; // it wrong way
x = GenderCopy[1];

// console.log("x", GenderCopy[1], GenderCopy[2], GenderCopy[3]);

// Unknown ---------------
// it is data type 
let notSure: unknown = 4;
notSure = "maybe a string instead";

// OK, definitely a boolean
notSure = false;

let maybe: unknown;


maybe = "hello";
// maybe.split("")
// (maybe as string).split("");

// you can't assign like this
// let aString: string = maybe;

// you cant direct assign unknown to type 
// after type of check you can assign
if (typeof maybe === "string") {
    // TypeScript knows that maybe is a string
    let aString: string = maybe;
}


// Any ---------------
// compiler check at run time any method and properties
// losing type safety
// should try to avoid using any when not necessary.

let str: any = 4;
str.split(""); // wrong when compile

// Void ---------------
// You may commonly see this as the return type of functions that do not return a value

function warnUser(): void {
    console.log("This is my warning message");
}

let unusable: void = undefined;
// only if --strictNullChecks is not specified
unusable = null;

// Null and Undefined ---------------
let u: undefined = undefined;
let n: null = null;

// Never ---------------
// Function returning never must not have a reachable end point
function error(message: string): never {
    throw new Error(message);
}

// Object ---------------
// object is a type that represents the non-primitive type

function create(o: object | null): void {

}

// Type assertions ---------------
// it way to tell compiler you know better than compiler
// TypeScript with JSX, only as-style assertions are allowed.

// 1st
let someValue: unknown = "this is a string";
let strLength: number = (someValue as string).length;

// 2nd
let someValue1: unknown = "this is a string";
let strLength1: number = (<string>someValue).length;

// Note: Number, String, Boolean, Symbol and Object don't use as type bcz these are not primitive types. Instead, use the types number, string, boolean, object and symbol.






