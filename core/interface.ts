// it is way to checking structure with dataType.
// it helpful full describe our requirement.

// example 1
function printLabel(labeledObj: { label: string }) {
    console.log(labeledObj.label);
}

let myObj = { size: 10, label: "Size 10 Object" };
printLabel(myObj);


// example 2
interface labeledValue {
    label: string
}

function printLabelCopy(labeledObj: labeledValue) {
    console.log(labeledObj.label);
}
// printLabelCopy(myObj);

// Optional Properties ---------------
// using "?" symbol you can make optional properties
// it help full to describe these possibly available properties

enum Gender {
    Male,
    Female,
    other
}

interface Student {
    name: string,
    age: number,
    gender?: Gender
}

function createStudent(obj: Student): void {
    // process

    // if you add another properties in same object it gives you error
    // obj.rollNumber = 10;
}

let obj = {
    name: "john",
    age: 25,
    gender: Gender.Male
}

createStudent(obj);

// Readonly properties ---------------
// you can assign properties initial time only
interface Indian {
    readonly adharNumber: string;
}

let manoj: Indian = { adharNumber: "xxxx" };

// manoj.adharNumber = "xxx"; // it gives you error message

// what is different between readonly vs const
// readonly for properties and const for variable

// Excess Property Checks ---------------
// typescript check similar property name. so be carefully of it.

interface Animal {
    leg: number
    canSpeak?: boolean
}

// const dog: Animal = { leg: 4, canSpeaks: false } // it gives error
const cat: Animal = { leg: 4, canSpeaks: false } as Animal; // should right this

// index signature
// it helps to add unknown property 
// each interface have 2 index signature , we will take about latter
interface LivingBeings {
    head: number,
    [prop: string]: number,
    // [prop: string]: any,
}

const akash: LivingBeings = { head: 1, leg: 2, hand: 4 }; // here you can assign unknown property
akash.hand = 4;
// akash.name = "abc"; // it gives u error

// const bikas: LivingBeings = { leg: 4 }; // it is error bcz there should be available one common property.

// Note: should not over use index-signature in interface

// Function Types ---------------------

interface SearchFunc {
    (source: string, subString: string): boolean;
}

const searchText: SearchFunc = (source, subString) => {
    return true;
}

// if we are using normal function then example
let mySearch: SearchFunc, mySearchCopy: SearchFunc;

mySearch = function (source: string, subString: string) {
    let result = source.search(subString);
    return result > -1;
};

// properties are infer automatically
mySearchCopy = function (src, sub) {
    let result = src.search(sub);
    return result > -1;
};


// Indexable Types ------------
// you can add number as property name
interface StringArray {
    [index: number]: string;
}
let myArray: StringArray = ["Bob", "Fred"];

let myStr = myArray[0];

// error example

interface Test {
    [name: number]: string,
    [name: string]: string
}


interface NumberDictionary {
    [index: string]: string|number ;
    length: number; // ok, length is a number
    name: string; // error, the type of 'name' is not a subtype of the indexer
}

// not able to understood
// https://www.typescriptlang.org/docs/handbook/interfaces.html#indexable-types

// -------------------------- class type --------------------
// Implementing an interface--------------
// java and C# interface feature we can use here
// basically it describe public side

interface Animals {
    canFly: boolean;
    makeSound: () => void;
}

class Dog implements Animals {
    canFly: boolean = false;
    makeSound() {
        console.log("bhooo");
    }
}

// Difference between the static and instance sides of classes-------------
interface ClockConstructor {
    new(hour: number, minute: number): ClockInterface;
}

function createClock(ctor: ClockConstructor, hour: number, minute: number): ClockInterface {
    return new ctor(hour, minute);
}

interface ClockInterface {
    tick(): void;
}

class DigitalClock implements ClockInterface {
    constructor(h: number, m: number) { }
    tick() {
        console.log("beep beep");
    }
}

let digital = createClock(DigitalClock, 10, 60)

// another way
class AnalogClock implements ClockInterface {
    constructor(hour: number, minute: number) { }
    tick() {
        console.log("beep beep")
    }
    stop() {
        console.log("stopping")
    }
}
const Clock: ClockConstructor = AnalogClock;


let a1: ClockInterface = new Clock(10, 12);
// a1.stop(); // gives error
let a2: AnalogClock = new AnalogClock(10, 12);
// a2.stop(); // it works

// Extending Interfaces-------------
interface Shape {
    color: string;
}

interface Square extends Shape {
    sideLength: number;
}

let square = {} as Square;
square.color = "blue";
square.sideLength = 10;

interface Box extends Square, Shape {
    area: number;
}

let myBox = {} as Box;

myBox.color = "red";
myBox.sideLength = 10;
myBox.area = 100;

// Hybrid Types--------------
// we can declare mix type
// basically it requires in third party lib as per doc
interface Counter {
    (): void;
    interval: number;
    reset: number;
}

function getCounter(): Counter {
    let counter = function () { } as Counter;
    counter.interval = 1000;
    counter.reset = 10000;
    return counter;
}

// Interfaces Extending Classes --------------
// inherits the members of the class but not their implementations
// inherit even the private and protected members of a base class
// i don't known what is benefit of this 
class Control {
    private state: any;
}

interface SelectableControl extends Control {
    select(): void;
}

class Button extends Control implements SelectableControl {
    select() { }
}

class TextBox extends Control {
    select() { }
}

// gives error
// class ImageControl implements SelectableControl {
//     select() { }
// }






