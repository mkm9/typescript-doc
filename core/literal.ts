// here we have 3 types
// strings, numbers, and booleans

// strings literal----

type Easing = "ease-in" | "ease-out" | "ease-in-out";

function animate(dx: number, dy: number, easing: Easing) { }

animate(1, 2, 'ease-out');

// Numeric Literal Types ------------

type tileSize = 8 | 16 | 32;

interface tileSizeCopy { tileSize: 8 | 16 | 32 }

// Boolean Literal Types ----------

interface ValidationSuccess {
    isValid: true;
    reason: null;
}

interface ValidationFailure {
    isValid: false;
    reason: string;
}

type ValidationResult = ValidationSuccess | ValidationFailure;
