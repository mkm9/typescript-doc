// it same as javascript with some extra feature
//examples
function add(x, y) {
    return x + y;
}
const sum = (x, y) => {
    return x + y;
}

// it can capture variable outside of function scope
let z = 100;

function addZ(x) {
    return x + z;
}

// Type function

function subtract(x: number, y: number): number {
    return x - y;
}

// write the function type

let myAdd: (x: number, y: number) => number = function (x: number, y: number) {
    return x + y;
}

// Inferring the types
// it infer automatically

let myAdd2: (baseValue: number, increment: number) => number = function (x, y) {
    return x + y;
};

// Optional and Default Parameters
// every parameter is assumed to be required 
// parameter can’t be given null or undefined
function buildName(firstName: string, lastName: string) {
    return firstName + " " + lastName;
}

// buildName("Bob");
// buildName("Bob", "Adams", "Sr.");
buildName("Bob", "Adam");

// if use "?" it will be optional parameter

function createName(firstName: string, lastName?: string) {
    return firstName + " " + lastName
}

createName("Adam");
createName("Adam", "Bob");


function generateName(firstName = "Will", lastName: string) {
    return firstName + " " + lastName;
}

let result3 = generateName("Bob", "Adams");
// pass undefined to get the default initialized value
let result4 = generateName(undefined, "Adams");
// console.log(result4);

// Rest Parameters--------
// it is same as javascript
// it help to get multiple unknown parameter 
function getRests(firstName: string, ...restOfName: string[]) {
    return restOfName.join(" ");
}

// employeeName will be "Joseph Samuel Lucas MacKinzie"
let employeeName = getRests("Joseph", "Samuel", "Lucas", "MacKinzie");
// console.log(employeeName);

// -----------------------THIS--------------------
// http://yehudakatz.com/2011/08/11/understanding-javascript-function-invocation-and-this/
// for learning this is javascript

// here we will not talk about how this work with javascript.

let deck = {
    suits: ["hearts", "spades", "clubs", "diamonds"],
    cards: Array(52),
    createCardPicker: function () {
        // NOTE: the line below is now an arrow function, allowing us to capture 'this' right here
        return () => {
            let pickedCard = Math.floor(Math.random() * 52);
            let pickedSuit = Math.floor(pickedCard / 13);

            return { suit: this.suits[pickedSuit], card: pickedCard % 13 };
        };
    },
};

// above example this type is ANY

// this parameters ---------
// we can provide an explicit this parameter.
// this parameters are fake parameters that come first in the parameter list of a function

function f(this: void, val: string) {
    // make sure `this` is unusable in this standalone function
    return val;
}

// console.log(f("hello"));
interface Card {
    suit: string;
    card: number;
}

interface Deck {
    suits: string[];
    cards: number[];
    createCardPicker(this: Deck): () => Card;
}

let deck1: Deck = {
    suits: ["hearts", "spades", "clubs", "diamonds"],
    cards: Array(52),
    // NOTE: The function now explicitly specifies that its callee must be of type Deck
    createCardPicker: function (this: Deck) {
        return () => {
            let pickedCard = Math.floor(Math.random() * 52);
            let pickedSuit = Math.floor(pickedCard / 13);

            return { suit: this.suits[pickedSuit], card: pickedCard % 13 };
        };
    },
};

let cardPicker = deck1.createCardPicker();
let pickedCard = cardPicker();

// this parameters in callbacks-------------------
// could not understood this topic


// Overloads---------------
// return different based on different input
// it is not checking type strictly , be careful when you are writing inner logic
function getType(x: any): any {
    // Check to see if we're working with an object/array
    if (typeof x == "object") {
        return "object"
    }
    else if (typeof x == "number") {
        return x
    }
}


function findType(x: object): string;
function findType(x: number): number;
function findType(x: any): any {
    if (typeof x == "object") {
        return "object"
    } else if (typeof x == "number") {
        return x;
    }
}

// findType();
// getType();







